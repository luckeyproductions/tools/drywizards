cd $(dirname -- "$0")

git fetch upstream
git checkout -f upstream/master templates/
./dryer.sh templates/

cd templates/wizards/
rm classes/urho3d/urho3d.png
rm projects/urho3d/urho3d.png
mv classes/urho3d/* classes/dry/
mv projects/urho3d/* projects/dry/
rmdir projects/urho3d/
rmdir classes/urho3d/
git restore classes/dry/dry.png
git restore projects/dry/dry.png
git add --all
git commit -m "Synced"
git push
