%{Cpp:LicenseTemplate}\

#ifndef %{GUARD}
#define %{GUARD}

#include "luckey.h"

%{JS: Cpp.openNamespaces('%{Class}')}
@if '%{Base}'
class %{CN}: public %{Base}
@else
class %{CN}
@endif
{
	DRY_OBJECT(%{CN}, %{Base});

public:
@if '%{Base}' === 'Drawable'
	%{CN}(Context* context, unsigned char drawableFlags);
    static void RegisterObject(Context* context);

    void OnSetEnabled() override;
    void ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results) override;
    void Update(const FrameInfo& frame) override;
    void UpdateBatches(const FrameInfo& frame) override;
    void UpdateGeometry(const FrameInfo& frame) override;

    UpdateGeometryType GetUpdateGeometryType() override;
    Geometry* GetLodGeometry(unsigned batchIndex, unsigned level) override;
    unsigned GetNumOccluderTriangles()  override;
    bool DrawOcclusion(OcclusionBuffer* buffer) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;
@elsif %{isDryClass}
	%{CN}(Context* context);
@else
	%{CN}();
@endif
@if '%{Base}' === 'Component' || '%{Base}' === 'LogicComponent'
    void OnSetEnabled() override;
@if '%{Base}' === 'LogicComponent'
    void Start() override;
    void DelayedStart() override;
    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;
@endif

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;
@elsif '%{Base}' === 'Serializable'
    void OnSetAttribute(const AttributeInfo& attr, const Variant& src) override;
    void OnGetAttribute(const AttributeInfo& attr, Variant& dest) const override;
    const Vector<AttributeInfo>* GetAttributes() const override;
    const Vector<AttributeInfo>* GetNetworkAttributes() const override;
    bool Load(Deserializer& source) override;
    bool Save(Serializer& dest) const override;
    bool LoadXML(const XMLElement& source) override;
    bool SaveXML(XMLElement& dest) const override;
    bool LoadJSON(const JSONValue& source) override;
    bool SaveJSON(JSONValue& dest) const override;

    void ApplyAttributes()  override;
    bool SaveDefaultAttributes() const override;
    void MarkNetworkUpdate() override;
@elsif '%{Base}' === 'Resource' || '%{Base}' === 'ResourceWithMetadata'
    bool BeginLoad(Deserializer& source) override;
    bool EndLoad() override;
    bool Save(Serializer& dest) const override;
    bool SaveFile(const String& fileName) const override;
@endif

};
%{JS: Cpp.closeNamespaces('%{Class}')}
#endif // %{GUARD}\
