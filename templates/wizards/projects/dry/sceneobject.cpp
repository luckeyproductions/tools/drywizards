%{Cpp:LicenseTemplate}\

#include "sceneobject.h"

SceneObject::SceneObject(Context* context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::OnNodeSet(Node* node)
{
    if (!node)
        return;
}

void SceneObject::Set(const Vector3& position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::PlaySample(Sound* sample, float gain)
{
    Node* sampleNode{ GetScene()->CreateChild("Sample") };
    sampleNode->SetWorldPosition(node_->GetWorldPosition());
    SoundSource3D* sampleSource{ sampleNode->CreateComponent<SoundSource3D>() };
    sampleSource->SetSoundType(SOUND_EFFECT);
    sampleSource->SetAutoRemoveMode(REMOVE_NODE);
}

Vector3 SceneObject::GetWorldPosition() const
{
    return node_->GetWorldPosition();
}
