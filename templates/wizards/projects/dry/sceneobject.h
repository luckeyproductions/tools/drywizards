%{Cpp:LicenseTemplate}\

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "mastercontrol.h"

class SceneObject: public LogicComponent
{
    DRY_OBJECT(SceneObject, Object);
public:
    SceneObject(Context* context);

    virtual void Set(const Vector3& position);
    void OnNodeSet(Node* node) override;

    Vector3 GetWorldPosition() const;

protected:
    void Disable();
    void PlaySample(Sound* sample, float gain = 0.3f);

    float randomizer_;
};

#endif // SCENEOBJECT_H
