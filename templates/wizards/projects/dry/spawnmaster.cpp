%{Cpp:LicenseTemplate}\

#include "spawnmaster.h"

SpawnMaster::SpawnMaster(Context* context): Object(context)
{
}

void SpawnMaster::Activate()
{
    SubscribeToEvent(E_SCENEUPDATE, DRY_HANDLER(SpawnMaster, HandleSceneUpdate));
}
void SpawnMaster::Deactivate()
{
    UnsubscribeFromAllEvents();
}
void SpawnMaster::Clear()
{
}

void SpawnMaster::Restart()
{
    Clear();
    Activate();
}

void SpawnMaster::HandleSceneUpdate(StringHash /*eventType*/, VariantMap &eventData)
{
    const float timeStep{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };
}
