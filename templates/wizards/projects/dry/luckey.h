%{Cpp:LicenseTemplate}\

#ifndef LUCKEY_H
#define LUCKEY_H

#include <Dry/Audio/Audio.h>
#include <Dry/Audio/BufferedSoundStream.h>
#include <Dry/Audio/Sound.h>
#include <Dry/Audio/SoundSource.h>
#include <Dry/Audio/SoundSource3D.h>
#include <Dry/Container/HashBase.h>
#include <Dry/Container/HashMap.h>
#include <Dry/Container/Vector.h>
#include <Dry/Core/CoreEvents.h>
#include <Dry/Engine/Application.h>
#include <Dry/Engine/Console.h>
#include <Dry/Engine/DebugHud.h>
#include <Dry/Engine/Engine.h>
#include <Dry/Engine/EngineDefs.h>
#include <Dry/Graphics/AnimatedModel.h>
#include <Dry/Graphics/AnimationController.h>
#include <Dry/Graphics/Animation.h>
#include <Dry/Graphics/AnimationState.h>
#include <Dry/Graphics/Camera.h>
#include <Dry/Graphics/DebugRenderer.h>
#include <Dry/Graphics/DecalSet.h>
#include <Dry/Graphics/Graphics.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Material.h>
#include <Dry/Graphics/Model.h>
#include <Dry/Graphics/Octree.h>
#include <Dry/Graphics/OctreeQuery.h>
#include <Dry/Graphics/ParticleEffect.h>
#include <Dry/Graphics/ParticleEmitter.h>
#include <Dry/Graphics/Renderer.h>
#include <Dry/Graphics/RenderPath.h>
#include <Dry/Graphics/Skybox.h>
#include <Dry/Graphics/StaticModel.h>
#include <Dry/Graphics/StaticModelGroup.h>
#include <Dry/Graphics/Viewport.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/Input/InputEvents.h>
#include <Dry/Input/Input.h>
#include <Dry/IO/FileSystem.h>
#include <Dry/IO/Log.h>
#include <Dry/IO/MemoryBuffer.h>
#include <Dry/Math/MathDefs.h>
#include <Dry/Math/Plane.h>
#include <Dry/Math/Polynomial.h>
#include <Dry/Math/Sphere.h>
#include <Dry/Math/Vector2.h>
#include <Dry/Math/Vector3.h>
#include <Dry/Physics/CollisionShape.h>
#include <Dry/Physics/Constraint.h>
#include <Dry/Physics/PhysicsEvents.h>
#include <Dry/Physics/PhysicsWorld.h>
#include <Dry/Physics/RigidBody.h>
#include <Dry/Resource/ResourceCache.h>
#include <Dry/Resource/XMLFile.h>
#include <Dry/Scene/LogicComponent.h>
#include <Dry/Scene/Component.h>
#include <Dry/Scene/Node.h>
#include <Dry/Scene/SceneEvents.h>
#include <Dry/Scene/Scene.h>
#include <Dry/UI/Font.h>
#include <Dry/UI/Text.h>
#include <Dry/UI/UI.h>
#include <Dry/UI/UIEvents.h>
#include <Dry/2D/TileMap2D.h>
#include <Dry/2D/TmxFile2D.h>
#include <Dry/Scene/ValueAnimation.h>

#include <Dry/DebugNew.h>

#define FILES GetSubsystem<FileSystem>()
#define ENGINE GetSubsystem<Engine>()
#define TIME GetSubsystem<Time>()
#define CACHE GetSubsystem<ResourceCache>()
#define INPUT GetSubsystem<Input>()
#define GRAPHICS GetSubsystem<Graphics>()
#define RENDERER GetSubsystem<Renderer>()
#define AUDIO GetSubsystem<Audio>()

#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)


using namespace Dry;
namespace LucKey {

float Delta(float lhs, float rhs, bool angle = false);
float Distance(Vector3 from, Vector3 to, const bool planar = false, Vector3 normal = Vector3::UP);
float DistanceToPlane(Vector3 from, Vector3 normal = Vector3::UP, Vector3 origin = Vector3::ZERO);
IntVector2 Scale(const IntVector2 lhs, const IntVector2 rhs);
Vector2 Rotate(const Vector2 vec2, const float angle);
float RandomSign();
Color RandomColor();
Color RandomSkinColor();
Color RandomHairColor(bool onlyNatural = false);

float Sine(float x);
float Cosine(float x);

int Cycle(int x, int min, int max);
float Cycle(float x, float min, float max);
}

#endif // LUCKEY_H
