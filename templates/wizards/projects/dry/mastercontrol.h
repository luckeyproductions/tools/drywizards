%{Cpp:LicenseTemplate}\

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

namespace Dry {
class Node;
class Scene;
}

@if %{InputMaster}
class Player;
@endif

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

@if %{InputMaster}
    void AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(Vector3 pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player *player);
@endif

    // Setup before engine initialization. Modifies the engine paramaters.
    void Setup() override;
    // Setup after engine initialization.
    void Start() override;
    // Cleanup after the main loop. Called by Application.
    void Stop() override;
    void Exit();

private:
    void CreateScene();

    static MasterControl* instance_;
@if %{InputMaster}
    Vector< SharedPtr<Player> > players_;
@endif
    Scene* scene_;
};

#endif // MASTERCONTROL_H
