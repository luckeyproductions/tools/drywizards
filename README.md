# QtCreator [Dry](https://gitlab.com/luckeyproductions/dry) Wizards

Placing, or linking to (`ln -s`), the _templates_ folder inside the `~/.config/QtProject/qtcreator/` folder (in Linux) will add both an **Dry Project** and an **Dry C++ Class** to the _New File or Project_ dialog in QtCreator.

Feel free to fork and modify these wizards to your own liking or issue a PR.

### Class wizard

In case of the class wizard this will open a dialog allowing you to name your new class and specify its base class. Instead of typing the name of the base class there's also a drop down containing a selection of commonly inherited-from Dry classes to pick from.

### Project wizard

When creating a project with the wizard it should compile _after_ linking to the Dry directory from inside the project folder.

Don't forget to link to resource folders from inside each build directory as well.